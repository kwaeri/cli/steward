#
# SPDX-PackageName: kwaeri/steward
# SPDX-PackageVersion: 0.5.0
# SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
# SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
#

# Define required services
services:
  - mysql:5.7.23

# Define variables
variables:
  # Configure mysql environment variables (https://hub.docker.com/r/_/mysql/)
  MYSQL_DATABASE: nodekit_test
  MYSQL_ROOT_PASSWORD: TestPass777

# Define Stage description list in order
stages:
  - Build and test the Node module
  - Publish to NPM
  - Create a Release
  - Deploy GitLab Pages content

# Define script that should run before a job, either globally or per job
before_script:
  - apt-get update -qq && apt-get upgrade -y -qq && apt-get install -y -qq curl software-properties-common
  - curl -fsSL https://deb.nodesource.com/setup_current.x | bash -
  - apt-get install -y -qq nodejs

# Define cache details
cache:
  paths:
  - node_modules/

# Define Jobs
node_module:
  stage: Build and test the Node module
  script:
    - export NODE_OPTIONS=--no-experimental-fetch
    - export NODE_ENV=test
    - npm install
    - npm run build
    - npm run test
    - npm run coverage
  coverage: /All files[^|]*\|[^|]*\s+([\d\.]+)/
  artifacts:
    paths:
      - coverage/
  rules:
    - when: always

# Define Jobs
publish:
  stage: Publish to NPM                 # Create a separate publish job because release job can't install
  dependencies:                         # The required tools to publish to npm, but still require release job
    - node_module                       # because we need that release image for the release cli, which
  script:                               # doesn't allow us to install our req'd tools for buiding/publishing
    #- 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )' # Check if ssh-agent available, install if not
    #- mkdir -p ~/.ssh # Create .ssh configuration directory
    #- eval $(ssh-agent -s)  # Ensure we're running bash (prints PID of agent if so?)
    #- '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config' # Disable host checking, since user can't accept first connection.
    - echo -e "//registry.npmjs.org/:_authToken=${NPM_TOKEN}" >> .npmrc
    - npm install
    - npm run build
    - npm publish --access public       # TODO: REMOVE AFTER INITIAL PUBLISH
  rules:
    - if: $CI_COMMIT_TAG                 # Run this job when a tag is created
      when: $CI_COMMIT_REF_NAME="main"
      when: on_success                   # https://gitlab.com/kwaeri/developer-tools/-/ci/editor?tab=LINT_TAB

# Define Jobs
release:
  stage: Create a Release
  dependencies:
    - node_module
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: node_module
      artifacts: true
  before_script: []                     # Overrides global before_script
  script:
    - echo "Creating release"
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: '$CI_COMMIT_MESSAGE'
    tag_name: '$CI_COMMIT_TAG'
  rules:
    - if: $CI_COMMIT_TAG                 # Run this job when a tag is created

pages:
  stage: Deploy GitLab Pages content
  dependencies:
    - node_module
  script:
    - mkdir public/
    - mv coverage/lcov-report/ public/coverage/
    - touch index.html
    - echo "<html><head><title>@kwaeri/steward</title></head><body bgcolor='ffffff' text='000000' style='height:100vh; width:100%;'><p style='margin:0 auto;'>Looking for <a href="/coverage">Coverage information?</a></p></body></html>" > index.html
    - mv index.html public/index.html
  artifacts:
    paths:
      - public
    expire_in: 30 days
  only:
    - main